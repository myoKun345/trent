﻿using System;
using Gtk;

namespace treelist
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();
			MainWindow win = MainWindow.Create();
			Application.Run ();
		}
	}
}
