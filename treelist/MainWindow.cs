﻿using System;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

public partial class MainWindow: Gtk.Window
{
	[UI] TreeView fakeview;

	public static MainWindow Create ()
	{
		Builder mw = new Builder (null, "treelist.interfaces.MainWindow.glade", null);
		return new MainWindow (mw, mw.GetObject ("window").Handle);
	}

	protected MainWindow (Builder mw, IntPtr handle) : base (handle)
	{
		mw.Autoconnect (this);

		DeleteEvent += OnDeleteEvent;

		this.Title = "TreeView Sample";
		SetDefaultSize (250, 100);
		Remove (fakeview);
		fakeview = new TreeView ();
		var view = new TreeView ();
		SetupTreeView (fakeview);
		Add (view);
		view.Show ();
		this.ShowAll ();
	}

	private void SetupTreeView (TreeView view) {
		Type[] t = { typeof(string), typeof(string), typeof(string), typeof(string) };
		var listmodel = new ListStore (t);
		view.Model = listmodel;

		var a = new TreeViewColumn ();
		a.Title = "Account Name";
		a.AddAttribute (new CellRendererText(), "text", 0);
		var e = new TreeViewColumn ();
		e.Title = "Type";
		e.AddAttribute (new CellRendererText(), "text", 1);
		var b = new TreeViewColumn ();
		b.Title = "Balance";
		b.AddAttribute (new CellRendererText(), "text", 2);

		view.InsertColumn (a, 0);
		view.InsertColumn (e, 1);
		view.InsertColumn (b, 2);

		TreeIter i;

		i = listmodel.Append ();
		listmodel.SetValues (i, 0, "My Visacard", 1, "card", 2, "102,10");

		i = listmodel.Append ();
		listmodel.SetValues (i, 0, "My Mastercard", 1, "card", 2, "10,20");
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

}
