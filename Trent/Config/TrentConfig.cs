﻿using System;

namespace Trent
{
	public class TrentConfig
	{
		public string TorrentPath { get; set; } = System.Environment.GetFolderPath (Environment.SpecialFolder.Personal) + "/.trent/torrents/";
		public string DownloadsPath { get; set; } = System.Environment.GetFolderPath (Environment.SpecialFolder.Personal) + "/Downloads/";

		public int Port { get; set; } = 6889;

		public int UploadSlots { get; set; } = 10;
		public int MaxConnections { get; set; } = 300;
		public int MaxDownload { get; set; } = 0;
		public int MaxUpload { get; set; } = 0;
	}
}

