﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace Trent
{
	public sealed class ConfigStore
	{
		private readonly string _jsonPath;
		private readonly JsonSerializerSettings _settings;

		public ConfigStore (string path)
		{
			if (path == null)
				throw new ArgumentNullException (nameof (path));
			if (!File.Exists (path))
				File.WriteAllText (path, "{}");

			_settings = new JsonSerializerSettings ();
			_settings.Formatting = Formatting.Indented;

			_jsonPath = path;
		}

		public TrentConfig Load ()
		{
			return JsonConvert.DeserializeObject<TrentConfig>(File.ReadAllText(_jsonPath));
		}

		public void Save(TrentConfig config)
		{
			File.WriteAllText(_jsonPath, JsonConvert.SerializeObject(config));
		}
	}
}

