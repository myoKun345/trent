﻿using System;
using System.Collections.Generic;
using System.Web;
using OctoTorrent;

namespace Trent
{
	public class Magnet
	{
		public Uri MagnetURI { get; }
		public Dictionary<string, string> Args { get; } = new Dictionary<string, string> ();
		public InfoHash Hash { get; }
		public List<RawTrackerTier> Trackers { get; } = new List<RawTrackerTier> ();

		public Magnet (string uri)
		{
			MagnetURI = new Uri (uri);

			var c = MagnetURI.Query.Substring (1).Split ('&');

			foreach (var i in c) {
				var x = i.Split ('=');

				if (i.StartsWith ("xt=")) {
					var y = x [1].Split (':');

					if (y [1] == "tree") {
						Args.Add (y [1] + ":" + y[2], y [3]);
					} else
						Args.Add (y [1], y [2]);
				}
				if (i.StartsWith ("tr=")) {
					var q = new RawTrackerTier ();
					q.Add (x [1]);

					Trackers.Add (q);
				} //else
					//Args.Add (x [0], x [1]);
			}

			try {
				Hash = InfoHash.FromMagnetLink(MagnetURI.AbsoluteUri);
			} catch {
				
			}
		}
	}
}

