﻿using System;
using Gtk;
using System.Collections.Generic;
using OctoTorrent.Client;
using OctoTorrent.Common;
using OctoTorrent.Client.Encryption;
using OctoTorrent.Dht;
using OctoTorrent.Dht.Listeners;
using OctoTorrent.BEncoding;
using System.Net;
using System.IO;

namespace Trent
{
	static class Program
	{
		public static TrentConfig Config { get; private set; }
		public static List<TorrentManager> Torrents { get; private set; }
		public static MainWindow Center { get; private set; }
		private static ConfigStore _store;
		public static ClientEngine Engine { get; private set; }

		public static void Main (string[] args)
		{
			Application.Init ();

			Config = new TrentConfig ();
			_store = new ConfigStore (System.Environment.GetFolderPath (Environment.SpecialFolder.Personal) + "/.trent/trent.json");
			Config = _store.Load ();
			//_store.Save(Config);

			Torrents = new List<TorrentManager> ();

			Center = MainWindow.Create ();
			Center.Show ();
			Center.ShowAll ();

			StartEngine ();

			Application.Run ();
		}

		private static void StartEngine()
		{
			Torrent t = null;

			var es = new EngineSettings (Config.DownloadsPath, Config.Port);
			es.PreferEncryption = true;
			es.AllowedEncryption = EncryptionTypes.All;

			var td = new TorrentSettings (Config.UploadSlots, Config.MaxConnections, Config.MaxDownload, Config.MaxUpload);

			Engine = new ClientEngine(es);
			Engine.ChangeListenEndpoint (new IPEndPoint (IPAddress.Any, Config.Port));

			Engine.TorrentRegistered += AddTorrentToGUI;

			byte[] nodes = null;
			try
			{
				nodes = File.ReadAllBytes(System.Environment.GetFolderPath (Environment.SpecialFolder.Personal) + "/.trent/dht");
			}
			catch 
			{
			}

			DhtListener d = new DhtListener (new IPEndPoint (IPAddress.Any, Config.Port));
			DhtEngine dht = new DhtEngine (d);
			Engine.RegisterDht (dht);
			d.Start ();
			Engine.DhtEngine.Start (nodes);

			if (!Directory.Exists (Engine.Settings.SavePath))
				Directory.CreateDirectory (Engine.Settings.SavePath);

			if (!Directory.Exists (Config.TorrentPath))
				Directory.CreateDirectory (Config.TorrentPath);

			BEncodedDictionary fastResume;
			try
			{
				fastResume = BEncodedValue.Decode<BEncodedDictionary>(File.ReadAllBytes(System.Environment.GetFolderPath (Environment.SpecialFolder.Personal) + "/.trent/fast"));
			}
			catch {
				fastResume = new BEncodedDictionary ();
			}

			foreach (string file in Directory.GetFiles(Config.TorrentPath)) {
				if (file.EndsWith (".torrent")) {
					try {
						t = Torrent.Load(file);
					}
					catch {
						continue;
					}

					var m = new TorrentManager (t, Config.DownloadsPath, td);
					if (fastResume.ContainsKey (t.InfoHash.ToHex ()))
						m.LoadFastResume (new FastResume ((BEncodedDictionary)fastResume [t.InfoHash.ToHex ()]));

					Engine.Register (m);

					Torrents.Add (m);
					//m.PeersFound += ManagerPeersFound;
				}
			}
		}

		private static void AddTorrentToGUI(object s, TorrentEventArgs e)
		{
			var m = e.TorrentManager;

			if (m.Torrent != null) {
				Console.WriteLine (m.Torrent.Name);

				var iter = Center.Store.Append ();
				Center.Store.SetValue (iter, 0, m.Torrent.Name);
			} else {
				Console.WriteLine ("magnet torrent");

				var iter = Center.Store.Append ();
				Center.Store.SetValue (iter, 0, "{metadata loading}");
			}
		}
	}
}
