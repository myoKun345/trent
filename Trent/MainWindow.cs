﻿using System;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;

namespace Trent
{
	public partial class MainWindow: Gtk.Window
	{
		Builder builder;
	
		[UI] public TreeView t_list;
		[UI] public Button add;
		public ListStore Store;

		public MagnetWindow AddDialog { get; private set; }

		public static MainWindow Create ()
		{
			Builder mw = new Builder (null, "Trent.interfaces.MainWindow.glade", null);
			return new MainWindow (mw, mw.GetObject ("window").Handle);
		}

		protected MainWindow (Builder mw, IntPtr handle) : base (handle)
		{
			this.builder = mw;
	
			builder.Autoconnect (this);
			DeleteEvent += OnDeleteEvent;
			add.Pressed += OnAddEvent;
			Type[] t = { typeof(string) };
			Store = new ListStore(t);
			t_list.Model = Store;
			var w = new TreeViewColumn ();
			w.Title = "Torrent";
			w.AddAttribute (new CellRendererText (), "text", 0);
			t_list.AppendColumn (w);
		}

		protected void OnAddEvent (object sender, EventArgs a)
		{
			AddDialog = MagnetWindow.Create (builder);
			AddDialog.Show ();
		}

		protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
			Application.Quit ();
			a.RetVal = true;
		}
	}
}
