﻿using System;
using System.Net;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;
using OctoTorrent.Client;
using OctoTorrent;
using OctoTorrent.Common;

namespace Trent
{
	public partial class MagnetWindow : Window
	{
		[UI] public Button addFinish;
		[UI] public Entry magnetBox;

		private Magnet mResult;

		public static MagnetWindow Create (Builder b)
		{
			return new MagnetWindow (b, b.GetObject ("addDialog").Handle);
		}

		protected MagnetWindow (Builder mw, IntPtr handle) : base (handle)
		{
			mw.Autoconnect (this);
			addFinish.SetStateFlags (StateFlags.Insensitive, false);
			addFinish.ButtonPressEvent += OnFinishPress;
			magnetBox.Changed += OnMagnetBoxChange;
		}

		protected void OnMagnetBoxChange (object e, EventArgs a) 
		{
			var s = e as Entry;

			if (s.Text != null && s.Text != "") {
				if (s.Text.StartsWith ("magnet:?") && s.Text.Contains ("xt=urn:btih:")) {
					mResult = new Magnet (s.Text);
					if (mResult.Args ["btih"] != null) {
						var hash = mResult.Args ["btih"];
						if (hash.Length == 40 || hash.Length == 32) {
							addFinish.SetStateFlags (StateFlags.Normal, true);
							s.StyleContext.RemoveClass ("error");
						} else
							its_wrong (s);
					} else
						its_wrong (s);
				} else
					its_wrong (s);
			} else {
				addFinish.SetStateFlags (StateFlags.Insensitive, true);
				s.StyleContext.RemoveClass("error");
			}
		}

		protected void OnFinishPress (object s, EventArgs a) {
			var td = new TorrentSettings (Program.Config.UploadSlots, Program.Config.MaxConnections, Program.Config.MaxDownload, Program.Config.MaxUpload);
			var t = new TorrentManager (mResult.Hash, Program.Config.DownloadsPath, td, Program.Config.TorrentPath, mResult.Trackers);
			
			Program.Engine.Register (t);
			Program.Torrents.Add (t);

			this.Destroy ();
		}

		private void its_wrong (Entry s) {
			addFinish.SetStateFlags (StateFlags.Insensitive, true);
			s.StyleContext.AddClass("error");
		}
	}
}

